# Packages are optional: if they are not present, certain code samples are not compiled
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

find_package(MPI REQUIRED)      # Built-in in CMake
find_package(Threads REQUIRED)

include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

find_package(LibMesh REQUIRED)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 8_unstructured)
endif()

set(NAME ${DWARF_PREFIX}_libmesh)

if (MPI_FOUND AND LIBMESH_FOUND)
	enable_language(CXX)
    include_directories(${LIBMESH_INCLUDE_DIRS} ${MPI_INCLUDE_PATH})
    add_executable(${NAME} fem_system_ex4.cpp heatsystem.cpp)
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native")
    elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -xHost -std=c++14")
    endif()
    set_target_properties(${NAME} PROPERTIES CXX_STANDARD 14 CXX_STANDARD_REQUIRED YES)
    target_link_libraries(${NAME} ${LIBMESH_LIBRARIES} ${MPI_CXX_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})
    install(TARGETS ${NAME} DESTINATION bin)
    message("** Enabling '${NAME}': with LibMesh and MPI")
else()
    message("## Skipping '${NAME}': LibMesh or MPI support missing")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")

unset(NAME)
# ==================================================================================================
