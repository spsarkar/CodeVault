#include "CollectiveCommunicator.hpp"

void CollectiveCommunicator::Communicate(State* model) {
	if (commDistGraph_ == MPI_COMM_NULL) { MpiReportErrorAbort("Communicator not initialized"); }

	MPI_Neighbor_alltoallw(model,                     // sendbuf
	                       sizes_.data(),             // sendcounts
	                       sendDisplacements_.data(), // sdispl
	                       sendTypes_.data(),         // sendtypes
	                       model,                     // recvbuf
	                       sizes_.data(),             // recvcounts
	                       recvDisplacements_.data(), // rdispls
	                       recvTypes_.data(),         // recvtypes
	                       commDistGraph_             // comm
	                       );
}

MpiRequest CollectiveCommunicator::AsyncCommunicate(State* model) {
	if (commDistGraph_ == MPI_COMM_NULL) { MpiReportErrorAbort("Communicator not initialized"); }

	MPI_Request req;
	MPI_Ineighbor_alltoallw(model,                     // sendbuf
	                        sizes_.data(),             // sendcounts
	                        sendDisplacements_.data(), // sdispl
	                        sendTypes_.data(),         // sendtypes
	                        model,                     // recvbuf
	                        sizes_.data(),             // recvcounts
	                        recvDisplacements_.data(), // rdispls
	                        recvTypes_.data(),         // recvtypes
	                        commDistGraph_,            // comm
	                        &req);                     // request
	return MpiRequest{{req}};
}
