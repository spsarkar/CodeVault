#pragma once

#include <cstddef>
#include <memory>
#include <ostream>

#include "Communicator.hpp"
#include "MpiEnvironment.hpp"
#include "Tile.hpp"

class MpiWireworld {
	Tile tile_;
	std::unique_ptr<Communicator> comm_;

	void processArea(Coord start, Size size);

  public:
	MpiWireworld(const MpiEnvironment& env, const Configuration& cfg);
	friend std::ostream& operator<<(std::ostream& out, const MpiWireworld& g);
	void write() const;
	void simulateStep();
};
