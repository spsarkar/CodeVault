#include <chrono>
#include <cstdlib>
#include <iostream>

#include "Configuration.hpp"
#include "MpiEnvironment.hpp"
#include "MpiWireworld.hpp"

int main(int argc, char* argv[]) {
	const auto& starttime = std::chrono::system_clock::now();

	// we rely on the MPI implementation to correctly forward the arguments
	MpiEnvironment env(argc, argv);
	const auto cfg = Configuration::parseArgs(argc, argv, env);

	MpiWireworld ww(env, cfg);

	for (std::size_t i{0}; i < cfg.Generations; ++i) {
		ww.simulateStep();

		if ((i % 1000) == 0 && env.isMaster()) { // show progress
			std::cout << "iteration:" << i << '\n';
		}
	}
	// write output file if needed
	if (!cfg.OutputFilePath.empty()) { ww.write(); }

	if (env.isMaster()) {
		std::cout
		    << "Execution time:"
		    << std::chrono::duration<double>{std::chrono::system_clock::now() -
		                                     starttime}
		           .count()
		    << "s\n";
	}
	return EXIT_SUCCESS;
}
